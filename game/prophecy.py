import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt
import string

import re
import math
import nltk
from nltk.corpus import stopwords

from nltk.lm.preprocessing import padded_everygram_pipeline
from nltk.lm import MLE, Laplace
from nltk.lm import Vocabulary
from nltk.probability import LidstoneProbDist


from nltk.util import everygrams, pad_sequence

from nltk.probability import LidstoneProbDist

import warnings
warnings.filterwarnings('ignore')

print('The nltk version is {}.'.format(nltk.__version__))

## El texto contiene una columna con frases que son dialogos de los personajes
## de The lord of the rings
PATH = "https://gitlab.com/leito25/think_unity/-/raw/master/Taller1_Resources/lotr_scripts.csv"
dataframe = pd.read_csv(PATH)

# El dataframe tiene dos columnas char y dialog
dataframe = dataframe[['char','dialog']] #solo tomo dos columnas char / dialog
dataframe = dataframe.dropna() # borra todos los registros con campos incompletos
dataframe_text = dataframe[['dialog']] # solo tomo los registro de dialog que son frases
dataframe_char = dataframe[['char']] # characters

##
la_frase = ""
##

# Remover puntuación con la ayuda de la libreria string
def remove_punct(text):
    text_nopunct = "".join([char for char in text if char not in string.punctuation])# Descarta todos los signos de puntuación
    return text_nopunct.lower() ##pasar a minúscula

#se crea una nueva columna con las mismas frases pero limpias
dataframe_text['dialog_clean'] = dataframe_text['dialog'].apply(lambda x: remove_punct(x))

#mostrar la tabla
dataframe_text.head(5)
dataframe_text.sample(n=10)

# el corpus es toda la materia prima de texto
corpus = dataframe_text['dialog'].values

# Agregar inicio y fin de frase
frases = [["<s>"] + [word.lower() for word in v.split()] + ["</s>"] for v in corpus]

# el corpus es toda la materia prima de texto
corpus = dataframe_text['dialog'].values

# Agregar inicio y fin de frase
frases = [["<s>"] + [word.lower() for word in v.split()] + ["</s>"] for v in corpus]

with open('frases_lor.txt', 'w', encoding="utf-8") as f:
    for item in frases:
        f.write("%s\n" % item)
        print("%s\n" % item)




# FRASES


# PROMERO SE SACAN LAS FRASES DEL CORPUS ENE STE CASO LA COLUMNA DIALOG DEL DATAFRAME

frases = [["<s>"] + [word for word in v.split()] + ["</s>"] for v in dataframe_text['dialog']]
## AQUI SE CALCULA LA FRECUENCIA DE DISTRIBUCIÓN PARA GENERAR EL RELACIONAMIENTO 
frases_bigramas = nltk.FreqDist([b for v in frases for b in list(nltk.bigrams(v))])
#display(frases_bigramas) ##VISUALIZACIÓN DE LAS FRECUENCIAS.

# MÓDELO ESTOCASTICO
def estocastico(freqdist, keys=None):
    pivot = np.random.rand()
    acc = 0.
    if keys is None: 
        keys = freqdist.keys()
    else:
        pivot = pivot * np.sum([freqdist.freq(k) for k in keys])
        
    for key in keys:
        acc = acc + freqdist.freq(key) 
        if pivot < acc: return key

def llaves(freqdist, start: tuple):
    assert(type(start) is tuple)
    return [k for k in freqdist.keys() if start == k[:len(start)]]

def generador_bigrama(prev= None, max_length=100):
    verse = prev.upper().split() if prev else list()
    prev = (verse[-1],) if prev else ('<s>', )
    for i in range(max_length):
        keys = llaves(frases_bigramas, prev)
        curr = estocastico(frases_bigramas, keys)[-1]
        if curr == '</s>': break
        verse.append(curr)
        prev = (curr, )
    return ' '.join(verse)


for i in range(1):
    i =generador_bigrama()
    print(i)
    la_frase = i