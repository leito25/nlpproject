WIN_WIDTH = 1280
WIN_HEIGHT = 720

# drwing layers
GUI_LAYER = 7
MORDOR = 6
SWORD_STORE = 5
PLAYER_LAYER = 4
BLOCK_LAYER = 3
INTERIOR_GROUND_LAYER = 2
GROUND_LAYER = 1

# Player speed
PLAYER_SPEED = 3

#tile size
TILESIZE = 64

#Colors
RED = (255, 0, 0)
BLACK = (0, 0, 0)
DARK_GREEN = (15, 128, 55)
MAGENTA_KEY = (209, 13, 202)
WHITE = (255, 255, 255)

FPS = 60

# Tilemap
Tilemap = [
    'BBBBBBBBBBBBBBBBBBBB',
    'BFCFBBBBBBBBBBXXSXXB',
    'B.........T..BXXXXXB',
    'B............BXXXXXB',
    'B.......P....BXXXXXB',
    'B............BXXBBBB',
    'B...........HBXXBBBB',
    'BM.................B',
    'B..................B',
    'B..................B',
    'BBBBBBBBBBBBBBBBBBBB',
]

# lista de saludos y palabritas
greetings = ['hey there', 'hello', 'hi', 'Hai', 'hey!', 'hey']
question = ['How are you?', 'How are you doing?', 'how are you']
responses = ['Okay', "I'm fine"]
var1 = ['who made you', 'who created you']
var2 = ['I_was_created_by_Edward_right_in_his_computer.', 'Edward', 'Some_guy_whom_i_never_got_to_know.']
var3 = ['what time is it', 'what is the time', 'time']
var4 = ['who are you', 'what is you name']
cmd1 = ['open browser', 'open google']
cmd2 = ['play music', 'play songs', 'play a song', 'open music player']
cmd3 = ['tell a joke', 'tell me a joke', 'say something funny', 'tell something funny']
jokes = ['Can a kangaroo jump higher than a house? Of course, a house doesn’t jump at all.', 'My dog used to chase people on a bike a lot. It got so bad, finally I had to take his bike away.', 'Doctor: Im sorry but you suffer from a terminal illness and have only 10 to live.Patient: What do you mean, 10? 10 what? Months? Weeks?!"Doctor: Nine.']
cmd4 = ['open youtube', 'i want to watch a video']
cmd5 = ['tell me the weather', 'weather', 'what about the weather']
cmd6 = ['exit', 'close', 'goodbye', 'nothing']
close_words = ['exit', 'close', 'goodbye', 'nothing', 'thanks', 'enought']
cmd7 = ['what is your color', 'what is your colour', 'your color', 'your color?']
colrep = ['Right now its rainbow', 'Right now its transparent', 'Right now its non chromatic']
cmd8 = ['what is you favourite colour', 'what is your favourite color']
cmd9 = ['thank you']
repfr9 = ['youre welcome', 'glad i could help you']

sauron = ['you will die', 'stary away from here', 'this is my power']


resultadoCorpus = ['Corpus']

