from pygame.constants import MOUSEMOTION
from game.config import *
import pygame
import game.config
from game.maingame import *
import math
import random

import random
import datetime
import webbrowser
import pyttsx3
import wikipedia
from pygame import mixer
import speech_recognition as sr
from pyowm import OWM

import re

import game.prophecy

import sys

class Spritesheet:
    def __init__(self, file):
        self.sheet = pygame.image.load(file).convert()

    def get_sprite(self, x, y, width, height):
        sprite = pygame.Surface([width, height])
        sprite.blit(self.sheet, (0,0), (x, y, width, height))
        sprite.set_colorkey(BLACK)
        return sprite

class Spritesheet_Char:
    def __init__(self, file):
        self.sheet = pygame.image.load(file).convert()

    def get_sprite_char(self, x, y, width, height):
        sprite = pygame.Surface([width, height])
        sprite.blit(self.sheet, (0,0), (x, y, width, height))
        sprite.set_colorkey(MAGENTA_KEY)
        return sprite

class Player(pygame.sprite.Sprite):
    def __init__(self, game, x, y):

        self.game = game
        self._layer = PLAYER_LAYER
        self.groups = self.game.all_sprites
        pygame.sprite.Sprite.__init__(self, self.groups)

        self.x = x * TILESIZE
        self.y = y * TILESIZE
        self.width = TILESIZE
        self.height = TILESIZE

        # character movement
        self.x_change = 0
        self.y_change = 0

        # facing - para donde mira el character
        self.facing = 'down'

        # loading the image
        ### image_to_load = pygame.image.load("game/img/SingleSprite.png")
        ### self.image = pygame.Surface([self.width, self.height])
        ### self.image.set_colorkey(BLACK)
        ### self.image.blit(image_to_load, (0,0))
        self.image = self.game.character_spritesheet.get_sprite_char(0+(2*TILESIZE),0+(2*TILESIZE),self.width,self.height)

        self.rect = self.image.get_rect()
        self.rect.x = self.x
        self.rect.y = self.y


    def update(self):
        self.movement()

        self.animate()

        self.rect.x += self.x_change
        ##
        self.collide_blocks('x')
        self.collide_holy('x')
        self.collide_time('x')
        self.collide_help('x')
        self.collide_mordor('x')
        ##
        self.rect.y += self.y_change
        ##
        self.collide_blocks('y')
        self.collide_holy('y')
        self.collide_time('y')
        self.collide_help('y')
        self.collide_mordor('y')
        ##

        self.x_change = 0
        self.y_change = 0


    def movement(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_a]:
            self.x_change -= PLAYER_SPEED
            self.facing = 'left'
        if keys[pygame.K_d]:
            self.x_change += PLAYER_SPEED
            self.facing = 'right'
        if keys[pygame.K_w]:
            self.y_change -= PLAYER_SPEED
            self.facing = 'up'
        if keys[pygame.K_s]:
            self.y_change += PLAYER_SPEED
            self.facing = 'down'

    def speech_prophecy(self):
        random_greeting = random.choice(greetings)
        print(random_greeting)
        #self.game.engine.say(random_greeting)
        #game.prophecy.generador_bigrama()
        the_prhrase = game.prophecy.generador_bigrama()
        print(the_prhrase)
        final_prhase = "The prophecy says: ", the_prhrase
        self.game.engine.say(final_prhase)
        self.game.engine.runAndWait()

    def speech_time(self):
        print("Current date and time : ")
        _now = datetime.datetime.now()
        print(_now.strftime("The time is %H:%M"))
        self.game.engine.say(_now.strftime("The time is %H:%M"))
        self.game.engine.runAndWait()

    def collide_blocks(self, direction):
        if direction == "x":
            hits  = pygame.sprite.spritecollide(self, self.game.blocks, False)
            if hits:
                #self.speech_test()
                if self.x_change > 0:
                    self.rect.x = hits[0].rect.left - self.rect.width
                if self.x_change < 0:
                    self.rect.x = hits[0].rect.right
        
        if direction == "y":
            hits  = pygame.sprite.spritecollide(self, self.game.blocks, False)
            if hits:
                #self.speech_test()
                if self.y_change > 0:
                    self.rect.y = hits[0].rect.top - self.rect.height
                if self.y_change < 0:
                    self.rect.y = hits[0].rect.bottom

    def collide_holy(self, direction):
        if direction == "x":
            hits  = pygame.sprite.spritecollide(self, self.game.holy, False)
            if hits:
                #self.waitForAnswer()
                self.speech_prophecy()
                if self.x_change > 0:
                    self.rect.x = hits[0].rect.left - self.rect.width
                if self.x_change < 0:
                    self.rect.x = hits[0].rect.right
        
        if direction == "y":
            hits  = pygame.sprite.spritecollide(self, self.game.holy, False)
            if hits:
                #self.waitForAnswer()
                self.speech_prophecy()
                if self.y_change > 0:
                    self.rect.y = hits[0].rect.top - self.rect.height
                if self.y_change < 0:
                    self.rect.y = hits[0].rect.bottom

    def collide_time(self, direction):
        if direction == "x":
            hits  = pygame.sprite.spritecollide(self, self.game.ttime, False)
            if hits:
                #self.waitForAnswer()
                self.speech_time()
                if self.x_change > 0:
                    self.rect.x = hits[0].rect.left - self.rect.width
                if self.x_change < 0:
                    self.rect.x = hits[0].rect.right
        
        if direction == "y":
            hits  = pygame.sprite.spritecollide(self, self.game.ttime, False)
            if hits:
                self.speech_time()
                if self.y_change > 0:
                    self.rect.y = hits[0].rect.top - self.rect.height
                if self.y_change < 0:
                    self.rect.y = hits[0].rect.bottom

    def collide_help(self, direction):
        if direction == "x":
            hits  = pygame.sprite.spritecollide(self, self.game.help, False)
            if hits:
                self.waitForAnswer()
                if self.x_change > 0:
                    self.rect.x = hits[0].rect.left - self.rect.width
                if self.x_change < 0:
                    self.rect.x = hits[0].rect.right
        
        if direction == "y":
            hits  = pygame.sprite.spritecollide(self, self.game.help, False)
            if hits:
                self.waitForAnswer()
                if self.y_change > 0:
                    self.rect.y = hits[0].rect.top - self.rect.height
                if self.y_change < 0:
                    self.rect.y = hits[0].rect.bottom

    def collide_mordor(self, direction):
        if direction == "x":
            hits  = pygame.sprite.spritecollide(self, self.game.mordorbad, False)
            if hits:
                self.growl()
                if self.x_change > 0:
                    self.rect.x = hits[0].rect.left - self.rect.width
                if self.x_change < 0:
                    self.rect.x = hits[0].rect.right
        
        if direction == "y":
            hits  = pygame.sprite.spritecollide(self, self.game.mordorbad, False)
            if hits:
                self.growl()
                if self.y_change > 0:
                    self.rect.y = hits[0].rect.top - self.rect.height
                if self.y_change < 0:
                    self.rect.y = hits[0].rect.bottom

    def growl(self):
        self.game.sound_growl.set_volume(0.5)
        self.game.sound_growl.play()
        self.game.engine.say(random.choice(sauron))
        self.game.engine.runAndWait()

    def waitForAnswer(self):
        random_greeting = random.choice(greetings)
        print(random_greeting)
        self.game.engine.say(random_greeting)
        self.game.engine.runAndWait()

        #aqui viene la parte del microfono
        now = datetime.datetime.now()
        self.game.r = sr.Recognizer()
        with sr.Microphone() as source:
            print("Give a term: ")
            self.game.engine.say("What do you want to know?, give me a term")
            self.game.engine.runAndWait()
            audio = self.game.r.listen(source)
            try:
                print("You said " + self.game.r.recognize_google(audio))

            except sr.UnknownValueError:
                print("Couldnt undestant")
                self.game.engine.say('I didnt get that. Try again')
                self.game.engine.runAndWait()
                return True

        if self.game.r.recognize_google(audio) in close_words:
            print('see you later')
            self.game.engine.say('see you later')
            self.game.engine.runAndWait()
            pass
        else:
            self.game.engine.say("please wait, looking for information")
            self.game.engine.runAndWait()
            searching_term = self.game.r.recognize_google(audio)

            try:
                p = wikipedia.summary(searching_term)
            except wikipedia.DisambiguationError as e:
                s = random.choice(e.options)
                p = wikipedia.summary(s)
                
            except wikipedia.PageError as e:
                s = random.choice(e.options)
                p = wikipedia.summary(s)

            the_summary = p
            cut_summary = re.split(r"\.\s*", the_summary)
            piece = (cut_summary[0], cut_summary[1])
            print("CUt:")
            print(piece)
            print("WHOLE:")
            print(the_summary)
            self.game.engine.say(piece)
            self.game.engine.runAndWait()
            #userInput3 = input("or else search in google")
            #webbrowser.open_new('www.google.com/search?q=' + userInput3)
            
        # código de reconocimiento de las letras
        '''if self.game.r.recognize_google(audio) in greetings:
            random_greeting = random.choice(greetings)
            print(random_greeting)
            self.game.engine.say(random_greeting)
            self.game.engine.runAndWait()
        elif self.game.r.recognize_google(audio) in question:
            self.game.engine.say('I am fine')
            self.game.engine.runAndWait()
            print('I am fine')
        elif self.game.r.recognize_google(audio) in cmd6:
            print('see you later')
            self.game.engine.say('see you later')
            self.game.engine.runAndWait()
            pass'''

    def animate(self):
        down_anim = self.image = self.game.character_spritesheet.get_sprite_char(0+(2*TILESIZE),0+(2*TILESIZE),self.width,self.height)
        up_anim = self.image = self.game.character_spritesheet.get_sprite_char(0+(2*TILESIZE),0+(0*TILESIZE),self.width,self.height)
        left_anim = self.image = self.game.character_spritesheet.get_sprite_char(0+(2*TILESIZE),0+(3*TILESIZE),self.width,self.height)
        right_anim = self.image = self.game.character_spritesheet.get_sprite_char(0+(2*TILESIZE),0+(1*TILESIZE),self.width,self.height)
        if self.facing == 'down':
            self.image = down_anim
        if self.facing == 'up':
            self.image = up_anim
        if self.facing == 'left':
            self.image = left_anim
        if self.facing == 'right':
            self.image = right_anim

            


# stage tiles - cración de los bloques que forman el nivel
class Block(pygame.sprite.Sprite):
    def __init__(self, game, x, y):
        self.game = game
        self._layer = BLOCK_LAYER
        self.groups = self.game.all_sprites, self.game.blocks
        pygame.sprite.Sprite.__init__(self, self.groups)

        self.x = x * TILESIZE
        self.y = y * TILESIZE
        self.width = TILESIZE
        self.height = TILESIZE

        self.image = self.game.terrain01_spritesheet.get_sprite(0+(4*TILESIZE),0+(6*TILESIZE),self.width,self.height)

        self.rect = self.image.get_rect()
        self.rect.x = self.x
        self.rect.y = self.y

# stage tiles - cración de los bloques que forman el nivel
class Ground(pygame.sprite.Sprite):
    def __init__(self, game, x, y):
        self.game = game
        self._layer = GROUND_LAYER
        self.groups = self.game.all_sprites
        pygame.sprite.Sprite.__init__(self, self.groups)

        self.x = x * TILESIZE
        self.y = y * TILESIZE
        self.width = TILESIZE
        self.height = TILESIZE

        #self.image = self.game.terrain01_spritesheet.get_sprite(0+(2*TILESIZE),0+(2*TILESIZE),self.width,self.height)
        self.image = self.game.terrain01_spritesheet.get_sprite(0+(6*TILESIZE),0+(14*TILESIZE),self.width,self.height)

        self.rect = self.image.get_rect()
        self.rect.x = self.x
        self.rect.y = self.y

# stage tiles - cración de los bloques que forman el nivel
class SwordSign(pygame.sprite.Sprite):
    def __init__(self, game, x, y):
        self.game = game
        self._layer = SWORD_STORE
        self.groups = self.game.all_sprites, self.game.holy
        pygame.sprite.Sprite.__init__(self, self.groups)

        self.x = x * TILESIZE
        self.y = y * TILESIZE
        self.width = TILESIZE
        self.height = TILESIZE

        self.image = self.game.terrain01_spritesheet.get_sprite(0+(8*TILESIZE),0+(12*TILESIZE),self.width,self.height*2)

        self.rect = self.image.get_rect()
        self.rect.x = self.x
        self.rect.y = self.y

# stage tiles - cración de los bloques que forman el nivel
class InteriorGround(pygame.sprite.Sprite):
    def __init__(self, game, x, y):
        self.game = game
        self._layer = INTERIOR_GROUND_LAYER
        self.groups = self.game.all_sprites
        pygame.sprite.Sprite.__init__(self, self.groups)

        self.x = x * TILESIZE
        self.y = y * TILESIZE
        self.width = TILESIZE
        self.height = TILESIZE

        self.image = self.game.terrain01_spritesheet.get_sprite(0+(5*TILESIZE),0+(2*TILESIZE),self.width,self.height)

        self.rect = self.image.get_rect()
        self.rect.x = self.x
        self.rect.y = self.y
        
        
# stage tiles - cración de los bloques que forman el nivel
class Mordor(pygame.sprite.Sprite):
    def __init__(self, game, x, y):
        self.game = game
        self._layer = MORDOR
        self.groups = self.game.all_sprites, self.game.mordorbad
        pygame.sprite.Sprite.__init__(self, self.groups)

        self.x = x * TILESIZE
        self.y = y * TILESIZE
        self.width = TILESIZE
        self.height = TILESIZE

        self.image = self.game.terrain01_spritesheet.get_sprite(0+(12*TILESIZE),0+(4*TILESIZE),self.width*4,self.height*3)

        self.rect = self.image.get_rect()
        self.rect.x = self.x
        self.rect.y = self.y

# Help
class Cross(pygame.sprite.Sprite):
    def __init__(self, game, x, y):
        self.game = game
        self._layer = GUI_LAYER
        self.groups = self.game.all_sprites, self.game.ttime
        pygame.sprite.Sprite.__init__(self, self.groups)

        self.x = x * TILESIZE
        self.y = y * TILESIZE
        self.width = TILESIZE
        self.height = TILESIZE

        
        self.image = self.game.terrain01_spritesheet.get_sprite(0+(TILESIZE),0+(13*TILESIZE),self.width,self.height*2)

        self.rect = self.image.get_rect()
        self.rect.x = self.x
        self.rect.y = self.y

# Help
class Flowers(pygame.sprite.Sprite):
    def __init__(self, game, x, y):
        self.game = game
        self._layer = GUI_LAYER
        self.groups = self.game.all_sprites, self.game.blocks
        pygame.sprite.Sprite.__init__(self, self.groups)

        self.x = x * TILESIZE
        self.y = y * TILESIZE
        self.width = TILESIZE
        self.height = TILESIZE

        self.image = self.game.terrain01_spritesheet.get_sprite(0+(5*TILESIZE),0+(4*TILESIZE),self.width,self.height)

        self.rect = self.image.get_rect()
        self.rect.x = self.x
        self.rect.y = self.y


# A kind of GUI


# Help
class BtnHelp(pygame.sprite.Sprite):
    def __init__(self, game, x, y):
        self.game = game
        self._layer = GUI_LAYER
        self.groups = self.game.all_sprites, self.game.help
        pygame.sprite.Sprite.__init__(self, self.groups)

        self.x = x * TILESIZE
        self.y = y * TILESIZE
        self.width = TILESIZE
        self.height = TILESIZE

        self.image = self.game.terrain01_spritesheet.get_sprite(0+(1*TILESIZE),0+(6*TILESIZE),self.width,self.height)

        self.rect = self.image.get_rect()
        self.rect.x = self.x
        self.rect.y = self.y

# Help
class SpriteHelp(pygame.sprite.Sprite):
    def __init__(self, game, x, y):
        self.game = game
        self._layer = GUI_LAYER
        self.groups = self.game.all_sprites, self.game.blocks
        pygame.sprite.Sprite.__init__(self, self.groups)

        self.x = x * TILESIZE
        self.y = y * TILESIZE
        self.width = TILESIZE
        self.height = TILESIZE

        self.image = self.game.terrain01_spritesheet.get_sprite(0,0+(9*TILESIZE),self.width*3,self.height*4)

        self.rect = self.image.get_rect()
        self.rect.x = self.x
        self.rect.y = self.y