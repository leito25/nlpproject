from game.sprites import *
from game.config import *
import sys

class Game:
    def __init__(self):
        pygame.init()
        self.screen = pygame.display.set_mode((WIN_WIDTH, WIN_HEIGHT))
        self.clock = pygame.time.Clock()
        self.font = pygame.font.Font("C:\Windows\Fonts\Arial.ttf", 32)
        self.running = True

        self.character_spritesheet = Spritesheet_Char("game/img/Character_m.png")
        self.terrain01_spritesheet = Spritesheet("game/img/Assets_01.png")
        self.terrain02_spritesheet = Spritesheet("game/img/Assets_01.png")

    # Crear el tilemap basados en las letras del nivel
    def createTilemap(self):
        for i, row in enumerate(Tilemap):
            for j, column in enumerate(row):
                Ground(self, j, i)
                if column == "B":
                    Block(self, j, i)
                if column == "P":
                    Player(self, j, i)
                if column == "S":
                    SwordSign(self, j, i)
                if column == "X":
                    InteriorGround(self, j, i)
                if column == "M":
                    Mordor(self, j, i)
                if column == "H":
                    BtnHelp(self, j, i)
                if column == "C":
                    Cross(self, j, i)
                if column == "F":
                    Flowers(self, j, i)
                if column == "T":
                    SpriteHelp(self, j, i)

    def new(self):
        self.soundObj = pygame.mixer.Sound('game/audio/cut_mixdown.wav')
        self.soundObj.set_volume(0.2)
        self.soundObj.play(-1)

        self.sound_growl = pygame.mixer.Sound('game/audio/sauro_growl.wav')

        
        #init voices
        # set up and calibrate the text to speech
        self.engine = pyttsx3.init()
        self.voices = self.engine.getProperty('voices')
        self.engine.setProperty('voice', self.voices[1].id)#0 SPA #1 ENG
        self.volume = self.engine.getProperty('volume')
        self.engine.setProperty('volume', 10.0)
        self.rate = self.engine.getProperty('rate')
        self.engine.setProperty('rate', self.rate - 25)



        # new game starts
        self.playing = True

        self.all_sprites = pygame.sprite.LayeredUpdates()
        self.blocks = pygame.sprite.LayeredUpdates()
        self.enemies = pygame.sprite.LayeredUpdates()
        self.attacks = pygame.sprite.LayeredUpdates()
        self.guilayer = pygame.sprite.LayeredUpdates()
        self.holy = pygame.sprite.LayeredUpdates()
        self.ttime = pygame.sprite.LayeredUpdates()
        self.help = pygame.sprite.LayeredUpdates()
        self.mordorbad = pygame.sprite.LayeredUpdates()

        # creación del mapa
        self.createTilemap()

        # create the GUI
        #self.btnArray = BtnHelp(self, 10, 2)




    def events(self):
        #loop of events
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONUP:
                pos = pygame.mouse.get_pos()
                #if self.btnArray.rect.collidepoint(pos):
                #    print("Core")


            if event.type == pygame.QUIT:
                self.playing = False
                self.running = False

    def update(self):
        # loop updates para todos los sprites
        self.all_sprites.update()

    def draw(self):
        # draw loop
        self.screen.fill(WHITE)
        self.all_sprites.draw(self.screen)
        self.clock.tick(FPS)
        pygame.display.update()

    def main(self):
        # game loop
        while self.playing:
            self.events()
            self.update()
            self.draw()

    def game_over(self):
        pass

    def intro_screen(self):
        pass




