


<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">
    <img src="https://media.utp.edu.co/img/optimized/marca_UTP.png" alt="Logo" width="150" height="40">
  </a>

  <h3 align="center">Game Assistant</h3>

  <p align="center">
    Natural Language Programming project.
    <br />
    
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">Resumen</a>
    <li><a href="#built-with">Introducción</a></li>
    </li>
    <li>
      <a href="#getting-started">Ejecución del proyecto</a>
    </li>
    <li>
    <a href="#usage">Platemamiento del problema y justificación</a></li>
    <li><a href="#roadmap">Objetivos</a></li>
    <li><a href="#contributing">Metodología</a></li>
    <li><a href="#license">Resultados y discusión</a></li>
    <li><a href="#contact">Conclusiones</a></li>
    <li><a href="#acknowledgements">Agradecimientos</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## Resumen

El siguiente es un proyecto que pretende sensibilizar al mundo del desarrollo de los videojuego en el tema de la programación de lenguaje natural y más concretamente en el uso de asistentes virtuales de voz que brinden una experiencia más enriquecedora pero también más inmersiva y usable para juegos que tienen contenidos textuales o de historia extensos, catálogos de objetos grandes e historias extensas.

También se busca dar una alternativa a la manera en que los individuos interactúan con sistemas interactivos,  ya sea entretenimiento o sean sistemas para terapias de personas con incapacidades que no le permitan utilizar los tradicionales dispositivos de entrada como control pads, joystick o teclados.

### Introducción

Por muchos años, dispositivos como el wiimote y el kinect de Xbox han querido extender el grado de interacción con el jugador, en casos como juegos de baile y de minijuegos, como también en el sonido como lo es Rockband en donde se simulan instrumentos de música como baterías y guitarras, sin ser guitarras completamente, han generado impacto pero no temas como el audio en tiempo real sólo ha tenido impacto en los actuales momentos donde muchos juegos son multijugador masivo.

Algunos intentos de mejorar la experiencia han sido aditamentos o plugins para los motores de juegos donde se presta un servicio de chat y de chatbots medianamente inteligentes que ayudan al usuario en el progreso pero solo de una manera logística más no inmersiva ni involucrada con el juego.

También es habitual la combinación de tecnologías muy popularizadas por youtubers donde se realizan partidas en tiempo real, en estas partidas lo que prevalece es la charla para la comunicación entre los jugadores pero tampoco hay una integración entre los servicios de voz, el juego y los jugadores.



<!-- GETTING STARTED -->
## Para empezar

El ejemplo está construido con python para escritorio ya que las funcionalidades de audio están ligadas al hardware, se recomienda tener los dispositivos de audio de entrada y salida es decir parlantes, micrófono o una diademas con micrófono.


## Ejecución del proyecto

Para ser ejecutado el proyecto se necesitan las siguientes librerias de python:
* Se debe tener python 3 instaldo ya sea por medio de la página de python o por conda de anaconda.
* Se sugiere tener python como global por medio de las variables de entorno.
* Tener instalado pip https://www.liquidweb.com/kb/install-pip-windows/
* La instalación de virtualenv se puede realizar ejecutando el sigueinte comando en cualquier carpeta del sistema:
  ```
  pip install virtualenv
  ```
* Se crea un entorno virtual con el siguiente comando:
  ```
  python -m venv env #donde env es la carpeta que se creara y contendrá el entorno que es una instalación de python para hacer pruebas.
  ```
* dentro de la carpeta env se crea una estructura de archivos en la cual se encuentra un script de nombre activate.bat, se activa el entorno con el comando:
  ```
  cd env\Scripts
  activate.bat
  ```
* De inmediato aparecera el nombre del entorno en la consola
  ```
  (env) D:\Path\Project\env\Scripts>
  ```
* En este momento ya se esta utilizando el entorno y se puede comprobar entrando al interprete de python con el comando -python- e importando un paquete que en el entorno local ya se tuviera instalado, el paquete no estará porque este es un entorno nuevo, el siguiente paso será instalar las librerias necesarias para correr el proyecto.
* Para instalar los modulos existe una manera muy practica y es por medio de una lista de todas la librerias necesarias, el archivo requirements se encuentra en la raiz del repositorio.
  ```
  pip install -r requirements.txt
  ```
* También es necesario descomprimir e instalar las voces sinteticas para windows - speechsdk51.exe.
* Este enlace tiene diferentes archivos de instalación para pyAudio https://stackoverflow.com/questions/52283840/i-cant-install-pyaudio-on-windows-how-to-solve-error-microsoft-visual-c-14
* Este es un repositorio de los instaladores
https://www.lfd.uci.edu/~gohlke/pythonlibs/#pyaudio

Los archivos whl se instalan de la sigueinte manera:
  ```
  pip install PyAudio-0.2.11-cp37-cp37m-win_amd64.whl
  ```
Finalmente se puede ejecutar el archivo mainfile.py con el comando:
  ```
  python mainfile.py
  ```

## Planteamiento del problema

La manera de consumir videojuegos ha cambiado, juegos como Halo 5 ya no tienen la opción de multijugador offline sino que viene listo para conectarse a los servidores de Xbox Live para tener partidas online. La interacción entre usuarios y la cantidad de información se convierte en una nueva posibilidad de experimentar y emprender en nuevas maneras de comunicarse con el juego y con los demás jugadores.

Estas nuevas maneras de jugar llevan a que las comunicaciones sean diferentes y una de estas es el uso del speech recognizement y de el uso de lenguaje natural en la experiencia de juego.

## Objetivos

Los objetivos principales de este proyecto son:
* Motivar el uso de tecnologías de reconocimiento de voz, manejo de datos y procesamiento de lenguaje para enriquecer la experiencia de juego.
* Brindar una alternativa a los usuarios que quieren otras experiencias o para usuarios que tienen discapacidades que no le permitan utilizar los dispositivos habituales.
* Mezclar tecnologías para ver cómo pueden resultar en un producto o campo de investigación interesante.

## Metodología

Desde el inicio del curso uno de los objetivos era utilizar lo aprendido en el campo de los videojuegos, en algún momento existía la posibilidad de realizar algo con la librería pygame especializada en videojuegos e interacción pero con el paso del tiempo también empezó a sentirse la necesidad de aplicarlo en algo nuevo como el uso de la voz y el habla sintética.

Lo primero fue investigar que existía en cualquier área del conocimiento sobre asistentes de voz y como era de imaginarse, en la actualidad los asistentes de smartphone como siri se han popularizado, pero ha sido tanto el que por el lado de Amazon y de Xiaomi se vende el dispositivo completo como un asistente, ya no viene mezclado con otro producto sino que es en sí un producto. Cabe agregar que el propósito del proyecto no era llegar a un producto como Alexa, pero sí poder experimentar.

Estas son las librerías que se utilizan en la parte de interacción con la voz y el audio además de las de la construcción del juego:
* Pyttsx3: text-to-speech conversation para python, a partir de un texto genera el sonido de una voz sintética.
* SpeechRecognition: Reconocimiento del habla del usuario para ser convertido en texto, es decir hace lo contrario a Pyttsx3, es necesario si queremos una interacción de dos vías con el player.
* Wikipedia: Librería de python que hace búsquedas en wikipedia, muy útil para sacar información.
* Pygame: Librería enfocada a crear videojuegos, en su núcleo funciona con SDL que se utiliza para dibujar objetos en pantalla. Además de manejar la parte gráfica también maneja eventos de interacción y sonido propio.

 
En la parte del procesamiento de lenguaje, el objetivo fue hacer uso de un corpus, para generar frases nuevas a partir del procesamiento de ese texto, esto con el fin de simular una interacción inmersiva basada en el tema del juego.

### La profecía
En este caso y partiendo de lo dicho al inicio, la idea era simular un entorno de un juego RPG(Juego de rol) del Señor de los anillos, asi que una mecánica de ejemplo es tener en el escenario una estatua a la cual se le piden profecías cada que se interactúa con ella. Esta estatua buscará en una base de datos procesada al inicio las mejores palabras y con ayuda de un algoritmo de métrica construirá una nueva frase.

Librerías utilizadas en esta mecánica:
* Pandas: Manejo de datasets, cortar, filtrar y demás utilidades.
* Numpy: Librería de cálculo numérico en especial matrices y vectores.
* Wikipedia: Librería de python que hace búsquedas en wikipedia, muy útil para sacar información.
* NLTK: La librería líder en procesamiento de lenguaje natural, esencial en este proyecto posee herramientas como el manejo de corpus y tokenizado además de otras más complejas.

### Diseño
Cabe agregar que es necesario presentar un diseño del prototipo construido por un lado está la parte del juego.
El Juego
El núcleo del proyecto comienza en el archivo mainfile.py donde se encuentra un llamado a la clase Game que maneja todo el juego, se crea una instancia de esa clase que tiene dentro de sí un gameloop que solo permite desactivarlo cuando se cierra la ventana.
El script maingame.py ubicado dentro de la carpeta game esta acompañado de otros dos scripts config.py el cual maneja las variables y objetos globales como colores, dimensiones, algunas instrucciones en forma de texto para la voz sintética y el mapa del juego que con ayuda de un algoritmo de reemplazo reemplaza caracteres por imágenes y así permite crear el mapa.
[Imagen del mapa](https://drive.google.com/file/d/1Bv-oTFlbtklrga5BHzKRNaCqbctgSWKp/view)

#### Estructura Juego
  ```
  mainfile.py
	game(folder)
		config.py
		maingame.py
		sprites.py
		prophecy.py
  ```
Continuando con el juego, en el script de sprites.py es donde se encuentra la mayor interacción ya que los sprites son las imágenes y estas interactúan entre si, generando eventos de colisión, por ejemplo cuando el player colisiona con el espantapájaros y este responde dando la hora la cual toma del sistema y devuelve en forma de audio con la ayuda de las librerías anteriormente mencionadas.

### La profecía, el diseño.

Para la profecía se tomó el siguiente corpus. https://gitlab.com/leito25/think_unity/-/raw/master/Taller1_Resources/lotr_scripts.csv, en el cual se encuentran dialogos de varios personajes del libro, en el proceso de creaciónde las nuevas frases estos fueron los pasos.

* Carga del Dataset por medio de pandas: Pandas es capaz de tomar un archivo en formato csv y organizarlo en una tabla en la cual se puede hacer diferentes operaciones, este cvs tiene varias columnas, de las cuales se tomaron las de char(personaje) y dialog(diálogo).
* Remover símbolos - Normalización: Para poder procesar los textos se elimina todo tipo de símbolos que no aportan al sentido de las palabras y también se deja el texto en minúscula para poder ser tratado.
* Tokenización: Con la ayuda de nltk se toman las frases y se descomponen en palabras y en secuencias.
* Frase: Para el trabajo de convertir el texto en n gramas primero se convierten en frase, esas son las frases base. 
* Stopwords: Las stop words en la mayoría de casos son eliminadas porque son esos conectores gramáticos que no tienen sentido son en el contexto de una oración (el, unos, eso, etc, en este caso se utilizan los stopwords del inglés )
* N Gramas: Son secuencias de palabras que sirven para crear nuevas secuencias y nuevas palabras a partir de la combinatoria que puedan generar, junto con otras técnicas se pueden crear palabras o frases de respuesta y por ello es necesario este paso en la creación de la profecía. Los n-gramas puede ser de 1-grama o unigrama, 2-gramas bigramas y n-gramas, en casos de creación puede ser útil un 3-gram o más pero en problemas de clasificación donde temas puede ser muy parecidos es a veces mejor un 2-gramas para no generar ambigüedades.
* FreqDist: Es una funcionalidad que proporciona nltk y es la de contar las ocurrencias, es decir las veces que aparece una palabra, al brindarnos la oportunidad de saber la probabilidad se puede en algún modelo combinatorio que genere nuevas frases.
* Modelo Estocastico: Es modelo estocástico significa que dependiendo de la probabilidad se pueden hacer combinaciones de palabras con esas distancias anteriormente consultadas, así los términos pueden ser los que a la vista del usuario puede ser los más comunes y así construir un vocabulario de palabras comunes. Las nuevas frases no son más que la respuesta a esos supuestos procesados a partir de unos bigramas.
* Generación del bigrama: En la generación del bigrama se hará el cálculo de cuales relacionamientos entre palabras son los mejores, por ejemplo las frases:
  ```
  ['<s>', 'welcome,', 'my', 'lords', 'to', 'isengard!', '</s>']
  ['<s>', 'my', 'master', 'sauron', 'the', 'great', 'bids', 'thee', 'welcome.', '</s>']
  ```
pueden hacer uso de la palabra welcome en otras frases nuevas,

  ```
  ['<s>', 'why', 'should', 'i', 'welcome', 'you......gandalf', '</s>']
  Why should I welcome you gandalf?
  ```
  
El modelo no tiene una procesamiento complejo es solo un ejercicio para comprender el procesamiento natural pero en ese camino se descubre un mundo de posibilidades al momento de enriquecer la experiencia en juegos que tienen mucho contenido como son los rpg.

Al final en el script de sprites.py donde se encuentran las interacciones le solicitamos al script prophecy.py que nos devuelva una frase, la cual es locutada por la voz sintética.

### La hora y unos datos más
Para hacer uso de las herramientas, se agrega a la interacción el espantapajaro que da la hora, por medio del api de python se solicita la hora y se le pasa codificada como una cadena al motor de habla el cual brinda la hora exacta del sistema.

El botón de Información tiene la funcionalidad de preguntar si quieres saber sobre un término específico, primero saluda al usuario y le dice que le diga un término, el usuario debe contestar y paso siguiente el sistema convierte el sonido en palabra y la busca en los términos que se tienen alojados en wikipedia.

Al terminar la búsqueda en wikipedia devuelve un texto del sumario que se encuentra del término y con ayuda de python se sacan solo dos renglones y se leen con la voz sintética. Esta funcionalidad puede ser explotada si en el juego hay una librería de objetos o hechizos, es decir cuando hay una base de datos gigantesca donde puede ser tedioso para el jugador ir a buscar en alguna parte del menú.
 
Este es un resumen del diseño del proyecto el cual busca mezclar las funcionalidades del speech con las mecánicas del juego.


## Objetivos
La investigación da como resultado un prototipo manipulable que da la sensación de un juego real y aunque se ha hablado sobre los beneficios, no se ha hablado todavía de las dificultades y tal vez desventajas al momento de implementarlas.
### Lo malo
* Una de las razones por las que no se populariza el uso de reconocimiento de voz y demás funcionalidades es porque el jugador a veces no tiene a la mano o no quiere utilizar dispositivos como micrófono y/o audífonos, esto deja de lado todo lo que se pueda brindar.
* La implementación versus el beneficio puede ser todavía desbalanceado ya que se necesita mucho tiempo para recoger datos, entrenarlos y brindarlos de la mejor manera, profesionalmente, en un juego se tendría que tener en cuenta la infraestructura donde corre todo el sistemas y eso se traduce en costos.
* Falta de interés por parte de los desarrolladores que se preocupan más por las mecánicas tradicionales y la interacción tradicional más que por cambiar la manera de jugar.
* Falta de conocimientos en el campo del lenguaje natural y por ende del manejo de big data e inteligencia artificial, para una compañía de videojuegos implementar un producto o agregarlo a un producto ya construido implica una inversión gigantesca que todavía muchos estudios no están dispuestos a tomar.
Falta de herramientas orientadas a videojuegos 
 
Estos puntos no demeritan la idea de abarcar estos temas, empresas como Unity Technologies ha entrado a este mercado con un producto como Vivox que es un servicio de comunicación de voz y texto. Una de las ventajas es brindar a los desarrolladores una plataforma multidispositivos, que funcionará de igual forma en móviles y escritorios. Pero además es multiplataforma entre motores de juego, la solución se puede implementar en varios motores o de manera nativa.

## Conclusiones
Al principio estaba muy perdido sobre cómo desarrollar algo interesante,  pero con el pasar de las clases y de los ejemplos vistos en clase se pudo ir desarrollando esta idea, en principio para mi es un paso gigante porque nunca le había prestado atención a procesamiento del lenguaje ni como puede ser mezclado con otras áreas para crear experiencias nuevas.

Creo que queda un gran camino por recorrer porque hacer más amigable la conversación significa más investigación, me parece sorprendente que las herramientas y python permitan por lo menos probar el uso de software tan especializado como lo es el speech recognizer y NLTK.

## Agradecimientos - referencias
* [1. Vivox from Unity Technologies.](https://en.wikipedia.org/wiki/Vivox)
* [2. Her movie - videogame scene.](https://www.youtube.com/watch?v=c8zDDPP3REE)
* [3. Vivox preview from Unite Now Event 2020.](https://www.youtube.com/watch?v=YbgK_xrlMck)
* [4. Halo 5 multiplayer only by xboxlive.](https://i.imgur.com/PtDU9fF.png)
* [5. Chatbot](https://towardsdatascience.com/)
* [6. Build a recognition bot.](how-to-build-a-speech-recognition-bot-with-python-81d0fe3cea9a)
* [7. Python jarvis development](https://www.youtube.com/watch?v=0sjRkz1UIDQ)
* [8. Game dev pygame](https://www.youtube.com/watch?v=crUF36OkGDw&t=639s)


